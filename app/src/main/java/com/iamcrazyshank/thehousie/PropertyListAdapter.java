package com.iamcrazyshank.thehousie;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.IOException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

public class PropertyListAdapter extends ArrayAdapter<Property> {


    //the list values in the List of type hero
    List<Property> propertyList;

    //activity context
    Context context;

    //the layout resource file for the list items
    int resource;

    public PropertyListAdapter(Context context, int resource, List<Property> propertyList) {
        super(context, resource, propertyList);
        this.context = context;
        this.resource = resource;
        this.propertyList = propertyList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {


        //we need to get the view of the xml for our list item
        //And for this we need a layoutinflater
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //getting the view
        View view = layoutInflater.inflate(resource, null, false);

        //getting the view elements of the list from the view
        ImageView imageView = view.findViewById(R.id.propImageView);
        TextView textViewName = view.findViewById(R.id.HeaderTextView);
        TextView textViewLocation = view.findViewById(R.id.LocationTextView);
        TextView textViewBedCount = view.findViewById(R.id.BedCountTextView);


        Button moreDetailbtn= (Button)view.findViewById(R.id.moreDetailsBtn);
        final Property prop = propertyList.get(position);

        AssetManager assetManager = context.getAssets();
        try {

            InputStream is = assetManager.open("properties/prop"+prop.getID()+"/"+ prop.getHeadImage());
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            imageView.setImageBitmap(bitmap);
        }catch (IOException e) {
            e.printStackTrace();
            Writer writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            String s = writer.toString();
            Log.e("Exception", s);

        }

        textViewName.setText(prop.getPropertyName());
        textViewLocation.setText(prop.getLocation());
        textViewBedCount.setText("Beds : " + prop.getBedCount());
        moreDetailbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.e("Clicked Property Item", prop.getID());
                Intent GotoDetails=new Intent(parent.getContext(), HousiePropertyDetail.class);
                GotoDetails.putExtra("selectedProperty",prop);
                parent.getContext().startActivity(GotoDetails);

            }
        });

        //finally returning the view
        return view;
    }
}
