package com.iamcrazyshank.thehousie;
import org.xmlpull.v1.XmlPullParserFactory;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class HousiePropertyList extends AppCompatActivity {
    //Creating a  object of pullParser
    private XmlPullParserFactory xmlPullParserFact;
    //a List of type property for holding property list
    List<Property> propList;
    //the listview
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        propList = new ArrayList<>();//declaration array list
        listView = (ListView) findViewById(R.id.myLineUpList);
        try {
            XmlPullParserHandler parser = new XmlPullParserHandler();
            //Get data.xml (Containing the xml data) to parse
            InputStream is = getAssets().open("data.xml");
            propList = parser.parse(is);
            //connect adapater to populate the list view with custom row view
            PropertyListAdapter adapter = new PropertyListAdapter(this, R.layout.property_item, propList);
            //attaching adapter to the listview
            listView.setAdapter(adapter);

        } catch (IOException e) {
            //catch parser exception
            e.printStackTrace();

        }


    }
}
