package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class HousiePropertyDetail extends AppCompatActivity {
    //declaring variables.
    private TextView PropertyName,PropertyCategory,PropertyType,FullAdress,ViewCount,DatePosted,Description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);

        //creating intent object
        Intent PropObj = getIntent();
        //get the bundle object
        final Property prop = (Property)PropObj.getSerializableExtra("selectedProperty");
        //Using ViewPager to connect image slider
        final ViewPager mViewPager = (ViewPager) findViewById(R.id.ImagesPager);
        //Button link for left and right navigation
        ImageButton leftNav = (ImageButton) findViewById(R.id.left_nav);
        ImageButton rightNav = (ImageButton) findViewById(R.id.right_nav);
        PropertyName = (TextView) findViewById(R.id.propertyNameTV);
        PropertyCategory = (TextView) findViewById(R.id.propCatTV);
        PropertyType = (TextView) findViewById(R.id.propTypeTV);
        FullAdress = (TextView) findViewById(R.id.propAddr);
        ViewCount = (TextView) findViewById(R.id.propViewsTV);
        DatePosted = (TextView) findViewById(R.id.propDatePosted);
        Description = (TextView) findViewById(R.id.propDescTV);
        //Creating a custom image adapter to make image slider.
        PropertyImageAdapter PagerAdapter = new PropertyImageAdapter(this, prop);
        mViewPager.setAdapter(PagerAdapter);

        // Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = mViewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    mViewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    mViewPager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = mViewPager.getCurrentItem();
                tab++;
                mViewPager.setCurrentItem(tab);
            }
        });
        //setting all the property details
        PropertyName.setText(prop.getPropertyName());
        PropertyCategory.setText(prop.getCategory());
        PropertyType.setText(prop.getType());
        FullAdress.setText(prop.getFullAddress());
        ViewCount.setText(prop.getViewCount());
        DatePosted.setText(prop.getDatePosted());
        Description.setText(prop.getDesc());

        //creating Owner detail button to navigate to Owner details page
        Button ownerDetailsBtn = (Button) findViewById(R.id.contactBtn);
        ownerDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating Intent
                Intent GotoOwnerDetails=new Intent(HousiePropertyDetail.this, HousieOwnerDetails.class);
                GotoOwnerDetails.putExtra("selectedProperty",prop);
                startActivity(GotoOwnerDetails);

            }
        });

        //creating Property Location button to navigate to Location page
        Button PropertyLocationBtn = (Button) findViewById(R.id.LocationBtn);
        PropertyLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating Intent
                Intent GoToLocation=new Intent(HousiePropertyDetail.this, HousiePropertyLocation.class);
                GoToLocation.putExtra("selectedProperty",prop);
                startActivity(GoToLocation);

            }
        });


    }
}
