package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class HousieSplash extends AppCompatActivity {
    //Declare Length for displaying the HousieSplash screen
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        /* New Handler to start the Login-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Login-Activity. */
                Intent LoginMainIntent = new Intent(HousieSplash.this, HousieLogin.class);
                HousieSplash.this.startActivity(LoginMainIntent);
                //HousieSplash.finish() will destroy the HousieSplash screen from the activity stack
                HousieSplash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}
