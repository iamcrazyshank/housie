package com.iamcrazyshank.thehousie;

import java.io.Serializable;

//Model class for parsing the xml
//Property class

public class Property implements Serializable {

    public String id;
    public String Property_name;
    public String Head_Image;
    public String No_of_Beds;
    public String Location,Category,Type,Desc,FullAddress,Zip,DatePosted;
    public String Image1,Image2,Image3,ViewCount;
    public String OwnerName;
    public String OwnerMobile;
    public String OwnerEmail;


//getters
    public String getID() {
        return id;
    }

    public String getPropertyName() {
        return Property_name;
    }

    public String getHeadImage() {
        return Head_Image;
    }

    public String getLocation() {
        return Location;
    }

    public String getCategory() {
        return Category;
    }

    public String getDesc() {
        return Desc;
    }

    public String getType() {
        return Type;
    }

    public String getImage1() {
        return Image1;
    }

    public String getImage2() {
        return Image2;
    }

    public String getImage3() {
        return Image3;
    }

    public String getFullAddress() {
        return FullAddress;
    }

    public String getDatePosted() {
        return DatePosted;
    }

    public String getZip() {
        return Zip;
    }

    public String getViewCount() {
        return ViewCount;
    }

    public String getBedCount() {
        return No_of_Beds;
    }

    public String getOwnerName() {
        return OwnerName;
    }

    public String getOwnerMobile(){ return  OwnerMobile; }

    public String getOwnerEmail() {
        return OwnerEmail;
    }

    //setters

    public void setID(String id) {
        this.id = id;
    }

    public void setPropertyName(String name) {
        this.Property_name = name;
    }

    public void setHeadImage(String img) {
        this.Head_Image = img;
    }

    public void setLocation(String loc) {
        this.Location = loc;
    }

    public void setBedCount(String count) {
        this.No_of_Beds = count;
    }

    public void setViewCount(String VC) {
        this.ViewCount = VC;
    }

    public void setCategory(String Cat) {
        this.Category = Cat;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public void setDesc(String Des) {
        this.Desc = Des;
    }

    public void setFullAddress(String Addr) {
        this.FullAddress = Addr;
    }

    public void setDatePosted(String date) {
        this.DatePosted = date;
    }

    public void setZip(String zip) {
        this.Zip = zip;
    }

    public void setImage1(String img) {
        this.Image1 = img;
    }

    public void setImage2(String img) {
        this.Image2 = img;
    }

    public void setImage3(String img) {
        this.Image3 = img;
    }

    public void setOwnerEmail(String ownerEmail) {
        OwnerEmail = ownerEmail;
    }

    public void setOwnerName(String ownerName) {
        OwnerName = ownerName;
    }

    public void setOwnerMobile(String ownerMobile) {
        OwnerMobile = ownerMobile;
    }

    public String toString() {
        return " Id= "+id + "\n Location= " + Location + "\n Prop name = " + Property_name;
    }


}






