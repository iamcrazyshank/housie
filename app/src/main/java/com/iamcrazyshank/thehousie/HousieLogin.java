package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.PatternSyntaxException;

//HOUSiE Login page
public class HousieLogin extends AppCompatActivity {

    //declarations for username and password textfields
    private EditText usernameEditText;
    private EditText passEditText;
    //Object creation for connecting Sqlite database
    private UserDataBaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Connecting to the layout xml
        usernameEditText = (EditText) findViewById(R.id.RegisterUsernameET);
        passEditText = (EditText) findViewById(R.id.RegisterPassET);
        databaseHelper = new UserDataBaseHelper(this);
    }


    public void checkLoginCreds(View arg0) {

        final String uname = usernameEditText.getText().toString();
        if (!isValidUsername(uname)) {
            //Set error message for email field
            usernameEditText.setError("Invalid Username(Only characters & Dot(.)allowed)");
        }

        final String pass = passEditText.getText().toString();
        if (!isValidPassword(pass)) {
            //Set error message for password field
            passEditText.setError("Password cannot be empty");
        }

        if(isValidUsername(uname) && isValidPassword(pass))
        {//only if valid uname and password
            hideSoftKeyBoard();
            //dbHelper checkUser is method to check if username and password (Key pair value ) exists in DB for data duplication.
            if (databaseHelper.checkUser(usernameEditText.getText().toString()
                    , passEditText.getText().toString())) {

                Log.e("******", "Check database for user validation");
                // Validation Completed

                Toast.makeText(getApplicationContext(), "Welcome "+ usernameEditText.getText().toString(), Toast.LENGTH_LONG).show();
                Intent mainIntent = new Intent(HousieLogin.this, HousiePropertyList.class);
                HousieLogin.this.startActivity(mainIntent);
                HousieLogin.this.finish();


            } else {
                //if Username and password does not exist in Table
                Toast.makeText(getApplicationContext(), "You are not registered with us. Please HousieRegister.", Toast.LENGTH_LONG).show();
            }



       }
    }

        //method to show/hide keyboard
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open. if true, close.
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void onNewUserClick(View arg0) {
        //Intent to go to HousieRegister activity.
        //For new user registration
        Intent GotoRegisterPage=new Intent(HousieLogin.this, HousieRegister.class);
        HousieLogin.this.startActivity(GotoRegisterPage);

    }


    // validating username
    private boolean isValidUsername(String username) {
        //regex to match user name
        //1. username should be greater than 6
        //2. username must only have a-z A-Z  0-9 and Dot(.).
        try {
            if (username.matches("\\b[a-zA-Z][a-zA-Z0-9\\.]{6,}\\b")) {
                // Successful match for regex
                return true;

            } else {
                return false;
                // Does not match the regex, returns false.
                //Shows error

            }
        } catch (PatternSyntaxException ex) {
            // Invalid regex
            return false;
        }

    }

    // validating password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }
        return false;
    }
}
