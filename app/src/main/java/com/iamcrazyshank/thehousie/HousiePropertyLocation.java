package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HousiePropertyLocation extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_location);
        //get intent object for location of the property
        Intent PropObj = getIntent();
        final Property prop = (Property)PropObj.getSerializableExtra("selectedProperty");
        //connection to webview in layout
        WebView myWebView = (WebView)findViewById(R.id.LocationWebView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.getSettings().setBuiltInZoomControls(true);
        //get the location of the user
        myWebView.getSettings().setGeolocationEnabled(true);

        myWebView.setWebChromeClient(new WebChromeClient() {
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });
        //load webview and loading url of googlemaps with zipcode
        myWebView.loadUrl("http://maps.google.com/maps?q="+prop.getZip());



    }
}
