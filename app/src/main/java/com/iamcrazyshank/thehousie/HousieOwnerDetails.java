package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HousieOwnerDetails extends AppCompatActivity {


    private TextView OwnerName;
    private Button CallOwner,MailOwner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_details);

        Intent PropObj = getIntent();
        final Property prop = (Property)PropObj.getSerializableExtra("selectedProperty");

        OwnerName = (TextView) findViewById(R.id.ownerNameTV);
        CallOwner = (Button) findViewById(R.id.callBtn);
        MailOwner = (Button) findViewById(R.id.emailBtn);
        OwnerName.setText(prop.getOwnerName());

        CallOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+prop.getOwnerMobile()));
                startActivity(intent);

            }
        });

        MailOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{prop.getOwnerEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");//message is your details
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(HousieOwnerDetails.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }




}
