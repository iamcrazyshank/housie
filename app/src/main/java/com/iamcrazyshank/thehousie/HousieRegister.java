package com.iamcrazyshank.thehousie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.PatternSyntaxException;

//HOUSiE REGISTER PAGE
public class HousieRegister extends AppCompatActivity {

    //declarations for username and password textfields
    //Userdatabase helper object to access and connect DB
    private UserDataBaseHelper databaseHelper;
    private EditText usernameEditText,passEditText;

    //Housie User model class
    private HousieUser H_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameEditText = (EditText) findViewById(R.id.RegisterUsernameET);
        passEditText = (EditText) findViewById(R.id.RegisterPassET);

        databaseHelper = new UserDataBaseHelper(this);
        H_user = new HousieUser();


    }


    public void onClickRegisterUser(View arg0) {

        final String uname = usernameEditText.getText().toString();
        if (!isValidUsername(uname)) {
            //Set error message for email field
            usernameEditText.setError("Username should be atleast 6 chars!");
        }

        final String pass = passEditText.getText().toString();
        if (!isValidPassword(pass)) {
            //Set error message for password field
            passEditText.setError("Password cannot be empty");
        }

        if(isValidUsername(uname) && isValidPassword(pass))
        { Log.e("******", "Check database for user validation");
            hideSoftKeyBoard();
            //checkUser checks the username in DB exists or unique.
            if (!(databaseHelper.checkUser(usernameEditText.getText().toString()
                    , passEditText.getText().toString()))) {
                H_user.setName(usernameEditText.getText().toString());
                H_user.setPassword(passEditText.getText().toString());
                //addUser inserts the username and password to the database
                databaseHelper.addUser(H_user);
                Toast.makeText(getApplicationContext(), "You have successfully registered.", Toast.LENGTH_LONG).show();


            } else {
            //show error if username already exists
                Toast.makeText(getApplicationContext(), "Username already exists. Please try a new one", Toast.LENGTH_LONG).show();
            }
        }
    }

    //Show/Hide keyboard
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    //Button action to go back to login page
    public void onClickBackToLogin(View arg0) {

        Intent GotoLoginrPage=new Intent(HousieRegister.this, HousieLogin.class);
        HousieRegister.this.startActivity(GotoLoginrPage);
        HousieRegister.this.finish();

    }

    private boolean isValidUsername(String username) {
        //regex to match user name
        //1. username should be greater than 6
        //2. username must only have a-z A-Z  0-9 and Dot(.).
        try {
            if (username.matches("\\b[a-zA-Z][a-zA-Z0-9\\.]{6,}\\b")) {
                // Successful match for regex
                return true;

            } else {
                return false;
                // Does not match the regex, returns false.
                //Shows error

            }
        } catch (PatternSyntaxException ex) {
            // Invalid regex
            return false;
        }
    }

    // validating password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 4) {
            return true;
        }
        return false;
    }



}
