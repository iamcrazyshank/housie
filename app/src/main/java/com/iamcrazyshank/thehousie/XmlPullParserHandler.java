package com.iamcrazyshank.thehousie;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;



//XML pull parser
public class XmlPullParserHandler {

    private List<Property> Properties= new ArrayList<Property>();
    private Property prop;
    private String text;

    public List<Property> getProperties() {
        return Properties;
    }

    public List<Property> parse(InputStream is) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser  parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("Property_Details")) {
                            // create a new instance of prop
                            prop = new Property();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("Property_Details")) {
                            // add prop object to list
                            Properties.add(prop);
                        }else if (tagname.equalsIgnoreCase("id")) {
                            prop.setID(text);
                        }  else if (tagname.equalsIgnoreCase("Property_name")) {
                            prop.setPropertyName(text);
                        } else if (tagname.equalsIgnoreCase("Head_Image")) {
                            prop.setHeadImage(text);
                        }else if (tagname.equalsIgnoreCase("No_of_Beds")) {
                            prop.setBedCount(text);
                        }else if (tagname.equalsIgnoreCase("Location")) {
                            // Log.e("!!Exception", text);
                            prop.setLocation(text);
                        }else if (tagname.equalsIgnoreCase("Image1")) {
                            // Log.e("!!Exception", text);
                            prop.setImage1(text);
                        }else if (tagname.equalsIgnoreCase("Image2")) {
                            // Log.e("!!Exception", text);
                            prop.setImage2(text);
                        }else if (tagname.equalsIgnoreCase("Image3")) {
                            // Log.e("!!Exception", text);
                            prop.setImage3(text);
                        }else if (tagname.equalsIgnoreCase("Category")) {
                            // Log.e("!!Exception", text);
                            prop.setCategory(text);
                        }else if (tagname.equalsIgnoreCase("Desc")) {
                            // Log.e("!!Exception", text);
                            prop.setDesc(text);
                        }else if (tagname.equalsIgnoreCase("Type")) {
                            // Log.e("!!Exception", text);
                            prop.setType(text);
                        }else if (tagname.equalsIgnoreCase("Full_Address")) {
                            // Log.e("!!Exception", text);
                            prop.setFullAddress(text);
                        }else if (tagname.equalsIgnoreCase("Zip")) {
                            // Log.e("!!Exception", text);
                            prop.setZip(text);
                        }else if (tagname.equalsIgnoreCase("Date_Posted")) {
                            // Log.e("!!Exception", text);
                            prop.setDatePosted(text);
                        }else if (tagname.equalsIgnoreCase("No_of_Views")) {
                            // Log.e("!!Exception", text);
                            prop.setViewCount(text);
                        }else if (tagname.equalsIgnoreCase("Full_Name")) {
                            prop.setOwnerName(text);

                        }else if (tagname.equalsIgnoreCase("Email")) {
                            prop.setOwnerEmail(text);
                        }else if (tagname.equalsIgnoreCase("Mobile")) {
                          prop.setOwnerMobile(text);
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {e.printStackTrace();}
        catch (IOException e) {e.printStackTrace();}
        Log.e("@@Values", Properties.toString());

        return Properties;
    }



}
