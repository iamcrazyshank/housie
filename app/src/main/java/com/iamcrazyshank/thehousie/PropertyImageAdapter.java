package com.iamcrazyshank.thehousie;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.viewpager.widget.PagerAdapter;
import java.io.IOException;
import java.io.InputStream;

public class PropertyImageAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Property PropRes;

    public PropertyImageAdapter(Context context, Property resources) {
        mContext = context;
        PropRes = resources;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    //Setting the slider count to 3 for 3 image slider
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.images_pager_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.propImageView);
        AssetManager assetManager = mContext.getAssets();
       try{
           InputStream is1 = assetManager.open("properties/prop"+PropRes.getID()+"/"+ PropRes.getImage1());
           Bitmap bitmap1 = BitmapFactory.decodeStream(is1);
           InputStream is2 = assetManager.open("properties/prop"+PropRes.getID()+"/"+ PropRes.getImage2());
           Bitmap bitmap2 = BitmapFactory.decodeStream(is2);
           InputStream is3 = assetManager.open("properties/prop"+PropRes.getID()+"/"+ PropRes.getImage3());
           Bitmap bitmap3 = BitmapFactory.decodeStream(is3);
           Bitmap bitmapArray [] = {bitmap1,bitmap2,bitmap3};
           imageView.setImageBitmap(bitmapArray[position]);
           container.addView(itemView);

       }catch (IOException e) {

        //catch image setting exception(IO)
        }

        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
