# **Housie** ![](/housie.png "Header") 
## Assignment 1 -> Mobile Application Design (Android) 
### Shashank Chandran (119225074)
## Task: 
1. Description: This work requires to develop 4 Activity Android Application. 
- Activity to present a List
- Activity to Show details of the chosen one. 
- Activity to display more details. 
-  Activity to Open any web information. 

2. Technical Concepts in this project: -
- 	User custom class to deal with Data - Parse data from XML using XMLPullparser
- 	Show image and two text view in List(Custom) –
- 	Show multiple images using view pager.
- 	Make elegant design(UI/UX)



## Project Description:
- HOUSiE launches with a login screen. Users have to register and using those credentials, they can login. For authentication and registration, I have used SQLite Database to store user details locally. 

- User can then login to the page using those credentials. HOUSiE displays a custom list of properties with set of images and details. The property details list is populated using XMLPullParser from a XML file. 

- If the user is interested in a property, he has to click “more details” button. In the more details, users can view more pictures and slide through them (Using View pager). In this page, user can see more details about the property. 

- In the bottom of the screen, User have an option to see the owner details and check the location of the property.

- Owner details page has two options, to call or email the owner.
- Two intents have been created to make a phone call or send an email.

- Property location page will open a WebView with property location on google maps. User can view and get direction to that property.

## Screenshots and GIFs :
![](/housie3.gif "") 
![](/housie_5.gif "") 

